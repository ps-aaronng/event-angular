import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import {EventService} from '../app/shared/service/event.service'
import { EventDetailsDialog } from './shared/dialog/event-details-dialog/event-details-dialog';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[EventService]
})
export class AppComponent implements OnInit {
  title = 'event-angular';
  data: any = [];
  value: any = '';
  constructor(
    private EventService : EventService,
    private dialog: MatDialog,
  ) {
  }

  ngOnInit(): void {
    this.EventService.getEvents().subscribe(data => {
      this.data = data?._embedded.events;
    });
  }


  getImage(event : any){
    return event.images[0].url;
  }

  notCoded(){
    window.location.replace("https://www.linkedin.com/in/aaron-ng-981059199/");
  }
  viewDetails(id:string){
    const dialogRef = this.dialog.open(EventDetailsDialog, {
      panelClass: "ng-dialog-container",
      data: {
       id:id
      },
    });
  }

}
