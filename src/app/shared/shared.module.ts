import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CustomPipeModule } from './pipe/pipe.module';
import { EventDetailsDialog } from './dialog/event-details-dialog/event-details-dialog';
import { MatCardModule } from '@angular/material/card';


@NgModule({
  declarations: [
    EventDetailsDialog,
  ],
  exports: [
    EventDetailsDialog,
  ],
  imports: [
    CommonModule,
    FormsModule,
    CustomPipeModule,
    RouterModule,
    MatCardModule,
  ],
})
export class SharedModule {}
